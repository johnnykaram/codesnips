/**
 * Created by johnny on 2016-01-21.
 */
var mYoutubeSearchMode = 1; // 0: add to playlist list   1: ADD TO MY SONGS

function CtrlYoutubeSearch($scope, $rootScope, $location, $state, $ionicPlatform) {

    setUpTransition($scope, $state, "#youtubeSearchView", $ionicPlatform);

    $scope.$on('$ionicView.loaded', function() {
        //var youtubeSearchField = $('#youtubeSearchView .search-bar-holder input.search');
        $scope.searchedTrackList = [];

        // enter event
        $('#youtubeSearchView .search-bar-holder input.search').keypress(function (e) {
            if (e.keyCode == 13) {
                searchYoutubeRequest();
            }
        });

        $('#youtubeSearchView .search-bar-holder input.search').autocomplete({
            source: function(request, response){
                /* google geli?tirici kimli?i (zorunlu de?il) */
                var apiKey = 'AIzaSyCGF_bcFqgzWQT6GVk1NHu88eYC0Xrk96w';
                /* aranacak kelime */
                var query = request.term;
                /* youtube sorgusu */
                $.ajax({
                    url: "http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+query+"&key="+apiKey+"&format=5&alt=json&callback=?",
                    dataType: 'jsonp',
                    success: function(data, textStatus, request) {
                        response( $.map( data[1], function(item) {
                            return {
                                label: item[0],
                                value: item[0]
                            }
                        }));
                    }
                });
            },
            /* se�ilene i?lem yapmak i�in buray? kullanabilirsin */
            select: function( event, ui ) {
                //$('#youtubeSearchView .search-bar-holder input.search').text(ui.item.label);
                $scope.searchTags = ui.item.label;
                searchYoutubeRequest();
                //alert();
            }
        });
    });

    $scope.$on('$ionicView.beforeEnter', function(){
        $('#youtubeSearchView .search-bar-holder input.search').focus();
    });

    $scope.done = function()
    {
        showGroupView();
    }

    $scope.searchClicked = function()
    {
        searchYoutubeRequest();
    }

    $scope.addTrackClicked = function(item)
    {
        if (item.isAlreadyAdded) {
            showQuickToast("Track already added");
        } else {
            item.isAlreadyAdded = true;
            item.class = "already-added";
            //$scope.searchedTrackList[item.index].isAlreadyAdded = true;
            var searchItem = $scope.searchResultItems[item.index];
            var youtubeTrackInfo = builtYoutubeTrackInfo(searchItem);

            switch (mYoutubeSearchMode)
            {
                case 0:
                    if (pushSingleYoutubeSuggestionItem(youtubeTrackInfo) !== -1)
                    {
                        //$(id + '.user-list-view li[name="' + index + '"]').addClass('already-added');
                        showQuickToast("Added");
                    } else {
                        showQuickToast("Failed to add suggestion");
                        console.error("Failed to add suggestion")
                    }
                    break;
                case 1:
                    if (isFunction(window.addYoutubeTrackToSelection))
                        window.addYoutubeTrackToSelection({trackInfo: youtubeTrackInfo });
                    //$scope.$emit("broadcastingToAll", { eventName : "addYoutubeTrack", data : {trackInfo: youtubeTrackInfo }});
                    break;
            }
        }
    }

    function searchYoutubeRequest() {
        if ($scope.searchTags.length <= 2)
        {
            showQuickToast("Please provide more info");
        } else {
            searchByKeyWords($scope.searchTags);
        }
    }

    function searchByKeyWords(keyword)
    {
        doSearch(keyword);
        function doSearch(keyword) {
            var request = gapi.client.youtube.search.list({
                q: keyword,
                part: 'snippet',
                maxResults: 20, // Change results number here
                type: 'video' // Notice that type used is video only
            });

            request.execute(function(results) {
                // save result
                $scope.searchResultItems = results.items;

                // create info list
                $scope.searchedTrackList = [];
                for (var index in results.items){
                    $scope.searchedTrackList.push({
                        song_name: results.items[index].snippet.title,
                        song_artist: results.items[index].snippet.channelTitle,
                        youtube_thumbnail_img_url : results.items[index].snippet.thumbnails.medium.url,
                        index : index,
                        isAlreadyAdded: false
                    });
                }
                $scope.$apply();
            });
        }
    }
}


function builtYoutubeTrackInfo(youtubeItem)
{
    var trackInfoObj =
    {
        is_youtube_video: true,
        youtube_thumbnail_img_url : youtubeItem.snippet.thumbnails.medium.url,
        song_uid: youtubeItem.id.videoId,
        song_name: youtubeItem.snippet.title,
        song_artist: youtubeItem.snippet.channelTitle,
        song_length: -1, // IMPORTANT: we can only know the actual duration only when video is started to play
        song_full_local_data_path: "-1",
        user_uid: mMyInfo.id,
        user_name: mMyInfo.get("username"),
        user_image_url: getProfilePicUrl(mMyInfo, true),
        push_unix_time: getUnixTime(),
        track_chunk_url: youtubeItem.id.videoId,
        track_chunk_local_path: "-1",
        unique_device_id: "-1",
        audio_sample_rate: 44100,
        audio_nb_channels: 2,
        audio_bit_rate: 16
        //,
        //promoting_user_ids_array: ["-1"],
        //disliking_user_ids_array: ["-1"]

    };
    return trackInfoObj;

}

/*

 snippet: Object
 channelId: "UCy622plSwxeDz6MXZAmNXrA"
 channelTitle: "JerrysGuitarBar"
 description: "You can download the complete masterclass covering the whole of Dogs here: https://www.jerrysguitarbar.com/guitar-video-lessons/individual-songs/dogs/"
 liveBroadcastContent: "none"
 publishedAt: "2013-11-17T16:12:35.000Z"
 thumbnails: Object
 default: Object
 high: Object
 medium: Object
 __proto__: Object
 title: "How to Play the Acoustic Introduction to Dogs Pink Floyd"
 __proto__: Obje

 */

/*
 Receive list
 */
//function updateYoutubeListView(items) {
//    var id = "#youtubeSearchListview";
//    if (!items) {
//        $(id + ".user-list-view ul").html("");
//        return;
//    }
//    var  dataSetHtmlStr = "";
//    for (var index in items){
//        var row = prepUserRow(items[index], index);
//        dataSetHtmlStr = dataSetHtmlStr.concat(row);
//    }
//
//    $(id + ".user-list-view ul").html(dataSetHtmlStr);
//
//    /// MONITORING DATA TABLE ACTIONS
//    $(id + '.user-list-view li .action-btns a').click(function (e) {
//        event.stopPropagation();
//        var index = parseInt($(this).attr("name"));
//        if ($(this).hasClass('add')) {
//
//            if ($(id + '.user-list-view li[name="' + index + '"]').hasClass('already-added'))
//            {
//                alert("already added");
//                // TODO: show toast message
//            } else {
//                var youtubeTrackInfo = builtYoutubeTrackInfo(items[index]);
//                if (pushSingleYoutubeSuggestionItem(youtubeTrackInfo) !== -1)
//                {
//                    $(id + '.user-list-view li[name="' + index + '"]').addClass('already-added');
//                    // TODO: show toast message
//                } else {
//                    showQuickToast("Failed to add suggestion");
//                    console.error("Failed to add suggestion")
//                }
//            }
//        }
//    });
//    function prepUserRow(videoItem, index)
//    {
//        $(id + '.user-list-view .template li').attr('name', index);
//        $(id + '.user-list-view .template li .action-btns a').attr('name', index);
//        $(id + '.user-list-view .template li .name').text(videoItem.snippet.title);
//        $(id + '.user-list-view .template li .info').text(videoItem.snippet.channelTitle);
//        $(id + '.user-list-view .template li .top-info-holder img').attr('src', videoItem.snippet.thumbnails.medium.url);
//
//        var row = $(id + '.user-list-view .template').html();
//        return row;
//    }
//}
