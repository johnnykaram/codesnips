/**
 * Created by Jean Andre Karam on 2016-01-30.
 */

function CtrlInvitePeople($scope, $location, $window, $state, $ionicPlatform) {

    setUpTransition($scope, $state, "#invitePeopleView", $ionicPlatform);

    $scope.$on('$ionicView.beforeEnter', function(){
        refreshData();
    });

    $scope.$on('$ionicView.afterLeave', function(){
    });

    $scope.doRefresh = function()
    {
        refreshData();
    };

    $scope.inviteUserClicked = function(parseUser)
    {
        if (parseUser.isAlreadyInvited) {
            showQuickToast(parseUser.name + " has already been invited");
        } else {
            sendPushNotifInviteToUser(parseUser.id, mGroupParseObject.id, mGroupParseObject.get("group_name"));
            showQuickToast(parseUser.name + " has been invited");
            parseUser.isAlreadyInvited = true;
        }
    }

    $scope.done = function()
    {
        showGroupView();
    }

    /**
     * Refresh all data
     */
    function refreshData()
    {
        refreshNearbyUsers();
        refreshOnlineUsers();
        refreshOthers();
    }

    function extractDataFromParseUserArray(parseUserArray)
    {
        for (var index in parseUserArray)
        {
            parseUserArray[index].name = parseUserArray[index].get("username");
            parseUserArray[index].profile_pic = getProfilePicUrl(parseUserArray[index]);
            parseUserArray[index].isAlreadyInvited = false;
        }
    }

    function refreshNearbyUsers()
    {
        getNearByUsers(function(nearByUsersList, error){
            for (var i=0;i< nearByUsersList.length;i++){
                console.log(nearByUsersList[i].get("username") + " is at "+ getDistanceFromUser(nearByUsersList[i]) + " km")
            }
            if (!error) {
                for (var index in nearByUsersList)
                {
                    nearByUsersList[index].name = nearByUsersList[index].get("username");
                    nearByUsersList[index].profile_pic = getProfilePicUrl(nearByUsersList[index]);
                    nearByUsersList[index].distance = getDistanceFromUser(nearByUsersList[index]);
                    nearByUsersList[index].isAlreadyInvited = false;
                }
                $scope.nearbyUsers = nearByUsersList;
            }
        });
    }

    function refreshOnlineUsers()
    {
        getOnlineInterestUsers(function(users, error)
        {
            if (!error) {
                extractDataFromParseUserArray(users);
                $scope.activeUsers = users;
            }
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    function refreshOthers()
    {
        extractDataFromParseUserArray(mFollowingUsersArray);
        $scope.otherFriendsUsers = mFollowingUsersArray;
    }
}
