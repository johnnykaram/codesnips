/**
 * Created by johnny on 2016-03-08.
 */

var NavigationHistory = {
  
  mNaviHistViewsArray: [],
  mNavHistCurrViewIndex: -1,
  
  notifyNavHistoryNavigateForwardToView: function (viewName) {
      // home page is the base view. When we navigate to it we go back to base
      switch (viewName) {
          case "homePageView":
              clearUpHistory();
              break;
      }
      
      // Every view can only be present once in the history stack
      for (var i = 0; i <= mNavHistCurrViewIndex; ++i) {
          if (mNaviHistViewsArray[i] === viewName) {
              mNavHistCurrViewIndex = i;
              return;
          }
      }

      ++mNavHistCurrViewIndex;
      mNaviHistViewsArray[mNavHistCurrViewIndex] = viewName;
  },
  clearUpHistory: function ()
  {
      mNaviHistViewsArray = [];
      mNavHistCurrViewIndex = -1;
  },
  registerHardBackEventOverrideHandler: function (event) {
      navigateBackwardToViewAction();
      event.preventDefault();
  },
  navigateBackwardToViewAction: function ()
  {
      if (mNavHistCurrViewIndex <= 0) {
          console.log("history cant go further back");
          showQuickToast("This is as back as you can go");
          showViewBasedEnumName("homePageView", false /* from native*/, false /* notify history*/);
          return;
      }
      // decrement position
      mNavHistCurrViewIndex--;
      
      var previousView = mNaviHistViewsArray[mNavHistCurrViewIndex];
      var viewIsValid = true;
      
      // these view are ignored by history
      switch (previousView) {
          case "groupView":
          case "chatting":
          case "youtubeSearch":
              if (!isMeInGroupSession())
                  viewIsValid = false;
              break;
          default :
              break;
      }

      // filter view that should not be in history
      switch (previousView) {
          // these view can not be in history
          case "welcomeView":
          case "LoadingView":
          case "groupPreviewView":
              viewIsValid = false;
              break;
      }
      if (viewIsValid) {
          showViewBasedEnumName(previousView, false /* from native*/, false /* notify history*/);
      } else {
          navigateBackwardToViewAction(); // we are skipping this view
      }
  }
};
