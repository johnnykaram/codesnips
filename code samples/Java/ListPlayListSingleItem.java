/**
 * Created by Jean Andre Karam on 2015-11-22.
 * Copy rights, All rights reserved
 */

package appymood.lobe.soundsmartproto.musicBrowse.listComponents;

import appymood.lobe.R;
import appymood.lobe.soundsmartproto.Audio.TrackInfo;
import appymood.lobe.soundsmartproto.Audio.AudioLocalPlayManager.LocalPlayingPoolEnumType;

import appymood.lobe.UI.ComponentHandlers.CustomButtonHandler;
import appymood.lobe.UI.ComponentHandlers.CustomButtonHandler.CustomButtonClickedHandler;
import appymood.lobe.soundsmartproto.Fragments.SocialWebFragment;
import appymood.lobe.soundsmartproto.Helper;
import appymood.lobe.soundsmartproto.MainActivity;
import appymood.lobe.soundsmartproto.musicBrowse.HelperAlbumArt;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ListPlayListSingleItem extends ListSingleItem implements CustomButtonClickedHandler {

	// UI
	private ImageView mDragImageView;
    private CustomButtonHandler mRemoveBtnHandler;
    
    // control
    private int dragImagePosX;
    private int dragImagePosY;

	public ListPlayListSingleItem(Context context, TrackInfo trackInfo, ListSingleItemControl control, LocalPlayingPoolEnumType poolType) {
		super(context, trackInfo, control, poolType, ListItemTypeEnum.SINGLE_PLAYLIST_ITEM);
	}
	
	@Override
	public void adoptView(View v, int position)
	{
		super.adoptView(v, position);
		setUpUI(v);
	}
	
	private void setUpUI(View view)
	{
		if (MainActivity.getmPlaylistFragment().isLobeListEditable())
		{
			view.findViewById(R.id.dragImageView).setVisibility(View.VISIBLE);
			view.findViewById(R.id.menuAction).setVisibility(View.VISIBLE);
		} else {
			view.findViewById(R.id.dragImageView).setVisibility(View.GONE);
			view.findViewById(R.id.menuAction).setVisibility(View.GONE);
		}

		mDragImageView = (ImageView)view.findViewById(R.id.dragImageView);

      CustomButtonHandler mMenuActionBtnHandler = new CustomButtonHandler(view.getContext(), (ImageButton) view.findViewById(R.id.menuAction), R.drawable.settings_menu_circular_vertical_white_small, -1, -1, this);
      mMenuActionBtnHandler.resetEnable(true);

      ImageButton likeBtn = (ImageButton)view.findViewById(R.id.likeTrackRowButton);
      likeBtn.setOnTouchListener(Helper.onImageButtonTouchListenerGreyDepthEffect);
      likeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) context).getSocialWebFragment().mJavaToJsManager.promoteTrackInLobeList(-1, getTrackInfo().getmID());
			}
		});

		ImageButton dislikeBtn = (ImageButton)view.findViewById(R.id.dislikeTrackRowButton);
		dislikeBtn.setOnTouchListener(Helper.onImageButtonTouchListenerGreyDepthEffect);
		dislikeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) context).getSocialWebFragment().mJavaToJsManager.dislikeTrackInLobeList(-1, getTrackInfo().getmID());
			}
		});

		((TextView)view.findViewById(R.id.nblikeTrackTxtView)).setText(String.valueOf(SocialWebFragment.getNbUsersPromotingTrack(getTrackInfo().getmID())));
		((TextView)view.findViewById(R.id.nbDislikeTrackTxtView)).setText(String.valueOf(SocialWebFragment.getNbUsersDislikingTrack(getTrackInfo().getmID())));

		TextView titleTxtView = (TextView) view.findViewById(R.id.songNameTxtView);
		if (((MainActivity)context).getmPlaylistFragment().isCurrentlyLoaded(mTrackInfo.getmID())
				|| ((MainActivity)context).getAudioManager().isThisTrackLoaded(mPoolType, mTrackInfo)) {
			// make the title bold
			titleTxtView.setTypeface(null, Typeface.BOLD);
			view.findViewById(R.id.songLogoFrame).setVisibility(View.GONE);

			((TextView)view.findViewById(R.id.songNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeColorBackgroundThirdOrange));
			((TextView)view.findViewById(R.id.authorNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeColorBackgroundThirdOrangePrio2Darker));

		} else {
			// make the title normal
			titleTxtView.setTypeface(null, Typeface.NORMAL);
			view.findViewById(R.id.songLogoFrame).setVisibility(View.VISIBLE);

			((TextView)view.findViewById(R.id.songNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio1_2));
			((TextView)view.findViewById(R.id.authorNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio3));

		}

		if (!((MainActivity)context).getAudioManager().isThisTrackLoaded(mPoolType, mTrackInfo) ) {
			String albArtPath = HelperAlbumArt.getAlbumArtPath(context, mTrackInfo.getmAlbumId(), mTrackInfo.getmID(), mTrackInfo);

			if (mTrackInfo.ismIsYoutubeVideo()) {
				Picasso.with(context).load(mTrackInfo.getmVideoThumbnailImageUrl())
						.resize(300, 300)
						.error(R.drawable.music_wall_paper).into((ImageView) view.findViewById(R.id.songLogoView));

			} else if (albArtPath != null) {

				Picasso.with(context).load(Uri.parse("file://" + albArtPath))
						.resize(300, 300)
						.error(R.drawable.music_wall_paper).into((ImageView) view.findViewById(R.id.songLogoView));
			} else {
				Picasso.with(context).load(R.drawable.music_wall_paper)
						.resize(300, 300)
						.error(R.drawable.lobe_logo_square_dark_background_128).into((ImageView) view.findViewById(R.id.songLogoView));
			}

		}

		// place user picture
		if (mTrackInfo.getmUserOwnerPictureUrl() != null) {
			Picasso.with(context).load(mTrackInfo.getmUserOwnerPictureUrl())
					.resize(100, 100)
					.transform(new Helper.CircleTransform())
					.error(R.drawable.user_not_signed_in_grey).into((ImageView) view.findViewById(R.id.userPicView));
		} else {
			Picasso.with(context).load(R.drawable.user_not_signed_in_grey)
					.resize(100, 100)
					.transform(new Helper.CircleTransform())
					.into((ImageView) view.findViewById(R.id.userPicView));
		}

		// place user host name
		TextView userHostTxtView = (TextView) view.findViewById(R.id.userHostName);
		if (mTrackInfo.getmUserOwnerName() == null || mTrackInfo.getmUserOwnerPictureUrl().equals(""))
			userHostTxtView.setText("unknown");
		else
			userHostTxtView.setText(mTrackInfo.getmUserOwnerName());

	}


	@Override
	public void onCustomButtonClicked(int imageBtnId) {
		if (imageBtnId == R.id.menuAction)
        {
            View v = mListSingleItemControl.getViewByPosition(mPosition);
			mListSingleItemControl.displaySingleItemActionMenu(v.findViewById(R.id.menuAction), this); // mSettingsBtnHandler.mImageButton
        }
        super.onCustomButtonClicked(imageBtnId);
	}


	public int getInflateViewID(){
		return R.layout.music_playlist_row;
	}
}
