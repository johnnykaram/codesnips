/**
 * Created by Jean Andre Karam on 2015-11-22.
 * Copy rights, All rights reserved
 */

package appymood.lobe.soundsmartproto.musicBrowse.listComponents;

import appymood.lobe.R;
import appymood.lobe.soundsmartproto.Audio.AudioLocalPlayManager.LocalPlayingPoolEnumType;
import appymood.lobe.soundsmartproto.Audio.TrackInfo;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import appymood.lobe.UI.ComponentHandlers.CustomButtonHandler.CustomButtonClickedHandler;

public class ListSingleItem extends ListGenericItem implements CustomButtonClickedHandler {

	// UI
	protected TrackInfo mTrackInfo;
	
	// control
	protected LocalPlayingPoolEnumType mPoolType;
	
    // logo control
	protected ListSingleItemControl mListSingleItemControl = null;
    
	protected ListSingleItem(Context context, TrackInfo trackInfo, ListSingleItemControl control, LocalPlayingPoolEnumType poolType, ListItemTypeEnum listItemTypeEnum) {
		super(context, listItemTypeEnum);
		mTrackInfo = trackInfo;
		mListSingleItemControl = control;
		mPoolType = poolType;
	}
	
	@Override
	public void adoptView(View v, int position)
	{
		super.adoptView(v, position);
		setUpUI(v);
	}
	
	public ListSingleItem getItemInstance()
	{
		return this;
	}
	
	/**
	 * This is the common UI between the playlist item and the music item (all single Items)
	 */
	private void setUpUI(View view)
	{
		// setting track info UI such as title and other common shared fields
		TextView titleTxtView = (TextView) view.findViewById(R.id.songNameTxtView);
		titleTxtView.setText(mTrackInfo.getmName());

		TextView  artistTxtView = (TextView) view.findViewById(R.id.authorNameTxtView);
		if (mTrackInfo.getmArtist().equals("<unknown>"))
			artistTxtView.setText("");
		else 
			artistTxtView.setText(mTrackInfo.getmArtist());
	}

	public TrackInfo getTrackInfo()
	{
		return mTrackInfo;
	}

    @Override
    public String[] getStringFilter()
    {
        String str = mTrackInfo.getmName() + " " + mTrackInfo.getmArtist();
        return str.split(" ");
    }

	@Override
	public void onCustomButtonClicked(int imageBtnId) {
		// No action yet
	}

    @Override
    public boolean isFilterable(){
        return true;
    }


	public LocalPlayingPoolEnumType getPoolType()
	{
		return mPoolType;
	}


	public static interface ListSingleItemControl {
		boolean isSingleItemSelected(ListSingleItem singleItem);
		void setCurrSelectSingleItem(ListSingleItem singleItem);
		void playManualRequestFromSingleItem(ListSingleItem singleItem);
		void dataSetHasChangedCustom();
		void displaySingleItemActionMenu(View anchorView, final ListSingleItem singleItem);
		View getViewByPosition(int pos);
	}
}
