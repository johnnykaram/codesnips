/**
 * Created by Jean Andre Karam on 2015-11-22.
 * Copy rights, All rights reserved
 */

package appymood.lobe.soundsmartproto.musicBrowse.listComponents;

import appymood.lobe.soundsmartproto.Audio.AudioOutManager;
import appymood.lobe.soundsmartproto.Helper;
import appymood.lobe.soundsmartproto.HelperMedia;
import appymood.lobe.soundsmartproto.MainActivity;
import appymood.lobe.R;
import appymood.lobe.soundsmartproto.Audio.TrackInfo;
import appymood.lobe.soundsmartproto.Audio.AudioLocalPlayManager.LocalPlayingPoolEnumType;

import appymood.lobe.UI.ComponentHandlers.CustomButtonHandler;
import appymood.lobe.UI.ComponentHandlers.CustomButtonHandler.CustomButtonClickedHandler;
import appymood.lobe.soundsmartproto.TutorialDialog;
import appymood.lobe.soundsmartproto.musicBrowse.HelperAlbumArt;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class ListMusicSingleItem extends ListSingleItem implements CustomButtonClickedHandler{

    // control
    protected boolean isPresentInPlaylist = false;
	
	public ListMusicSingleItem(Context context, TrackInfo trackInfo, ListSingleItemControl control, LocalPlayingPoolEnumType poolType) {
		super(context, trackInfo, control, poolType, ListItemTypeEnum.SINGLE_lIB_ITEM);
	}
	
	@Override
	public void adoptView(View v, int position)
	{
		super.adoptView(v, position);
		setUpUI(v);
	}

	private void setUpUI(View view)
	{
		// show main views
		view.findViewById(R.id.moveUplistBtnHolder).setVisibility(View.VISIBLE);
    view.findViewById(R.id.menuAction).setVisibility(View.VISIBLE);

		setQuickPauseUI(view);

		// hiding setting button if in stream mode
		if (AudioOutManager.mLocalPlayingSource == AudioOutManager.PlayingSourceEnumType.PLAYING_FROM_STREAM)
		{
			view.findViewById(R.id.menuAction).setVisibility(View.GONE);
		} else {
			view.findViewById(R.id.menuAction).setVisibility(View.VISIBLE);
		}

		view.setOnClickListener(mOnViewClickListener);

		ImageButton button = (ImageButton) view.findViewById(R.id.menuAction);
		final ListSingleItem listMusicSingleItem = this;
		button.setOnTouchListener(Helper.onImageButtonTouchListenerGreyDepthEffect);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View viewPressed = mListSingleItemControl.getViewByPosition(mPosition);
				mListSingleItemControl.displaySingleItemActionMenu(viewPressed.findViewById(R.id.menuAction), listMusicSingleItem); // mSettingsBtnHandler.mImageButton
				TutorialDialog.RequestTutorialCheck(context, TutorialDialog.mTutorialTypeEnum.insertIntoLobeList);
			}
		});

		TextView titleTxtView = (TextView) view.findViewById(R.id.songNameTxtView);
		if (((MainActivity)context).getAudioManager().isThisTrackLoaded(mPoolType, mTrackInfo)) {
			// make the title bold
			titleTxtView.setTypeface(null, Typeface.BOLD);
			// view.findViewById(R.id.songLogoFrame).setVisibility(View.GONE);
		} else {
			// make the title normal
			titleTxtView.setTypeface(null, Typeface.NORMAL);
			// view.findViewById(R.id.songLogoFrame).setVisibility(View.VISIBLE);
		}

		// setting Q-ing events
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				QToggleClicked();
			}
		};
		Button bQ = (Button)view.findViewById(R.id.QueueItemButton);
		bQ.setOnClickListener(onClickListener);
		Button bUnQ = (Button)view.findViewById(R.id.unQueueItemButton);
		bUnQ.setOnClickListener(onClickListener);
		view.findViewById(R.id.moveUplistBtnHolder).setOnClickListener(onClickListener);


		// adapt ui to item state
		isPresentInPlaylist = MainActivity.getmPlaylistFragment().isThisTrackInPlaylist(mTrackInfo);
		if (isPresentInPlaylist) // is this track in the current opened playlist
		{
			((TextView)view.findViewById(R.id.songNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio5));
			((TextView)view.findViewById(R.id.authorNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio5));

			bUnQ.setVisibility(View.VISIBLE);
			bQ.setVisibility(View.GONE);
		} else {
			((TextView)view.findViewById(R.id.songNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio1_2));
			((TextView)view.findViewById(R.id.authorNameTxtView)).setTextColor(context.getResources().getColor(R.color.appThemeTextColorOnBackgroundPrio3));

			bUnQ.setVisibility(View.GONE);
			bQ.setVisibility(View.VISIBLE);

		}
	}


	private void setQuickPauseUI(View view)
	{
		if (((MainActivity)context).getAudioManager().isThisTrackPlaying(mPoolType, mTrackInfo)) // is this track playing
		{
			// show or hide play stop btn
			view.findViewById(R.id.quickPauseBtn).setVisibility(View.VISIBLE);

			ImageButton button = (ImageButton) view.findViewById(R.id.quickPauseBtn);
			button.setOnTouchListener(Helper.onImageButtonTouchListenerGreyDepthEffect);
			button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					((MainActivity)context).getAudioManager().pauseMusicEvent();
				}
			});
		} else
		{
			view.findViewById(R.id.quickPauseBtn).setVisibility(View.GONE);
		}
	}


	private void updateAlbumArt(View view)
	{
		if (!((MainActivity)context).getAudioManager().isThisTrackLoaded(mPoolType, mTrackInfo) ) {
			String albArtPath = HelperAlbumArt.getAlbumArtPath(context, mTrackInfo.getmAlbumId(), mTrackInfo.getmID(), mTrackInfo);
			if (albArtPath != null) {

				if (isPresentInPlaylist) {
					Picasso.with(context).load(Uri.parse("file://" + albArtPath))
							.resize(100, 100)
							.transform(new Helper.CircleTransformDarkened(3))
							.error(R.drawable.lobe_logo_square_dark_background_128).into((ImageView) view.findViewById(R.id.songLogoView));
				} else {
					Picasso.with(context).load(Uri.parse("file://" + albArtPath))
							.resize(100, 100)
							.transform(new Helper.TransformDarkened(context, 1))
							.error(R.drawable.lobe_logo_square_dark_background_128).into((ImageView) view.findViewById(R.id.songLogoView));
				}

			} else {
				if (isPresentInPlaylist) {
					Picasso.with(context).load(R.drawable.lobe_logo_square_dark_background_128)
							.resize(100, 100)
							.transform(new Helper.CircleTransformDarkened(3))
							.error(R.drawable.lobe_logo_square_dark_background_128).into((ImageView) view.findViewById(R.id.songLogoView));
				} else {
					Picasso.with(context).load(R.drawable.lobe_logo_square_dark_background_128)
							.resize(100, 100)
							.transform(new Helper.TransformDarkened(context, 1))
							.error(R.drawable.lobe_logo_square_dark_background_128).into((ImageView) view.findViewById(R.id.songLogoView));
				}
			}
		}
	}


	public OnClickListener mOnViewClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			mListSingleItemControl.setCurrSelectSingleItem(getItemInstance());
			mListSingleItemControl.dataSetHasChangedCustom();
		}
	};


	private void QToggleClicked()
	{
		if (isPresentInPlaylist) {
			MainActivity.getmPlaylistFragment().removeItemContainingTrackInfoFromList(mTrackInfo); // add the track to the playlist section
		} else {
			MainActivity.getmPlaylistFragment().addTrackFromLib(this); // add the track to the playlist section
		}
		TutorialDialog.RequestTutorialCheck(context, TutorialDialog.mTutorialTypeEnum.addToLobeList);
	}


	@Override
	public void onCustomButtonClicked(int imageBtnId) {
		super.onCustomButtonClicked(imageBtnId);
	}
	
	
}
