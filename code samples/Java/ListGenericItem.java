/**
 * Created by Jean Andre Karam on 2015-11-22.
 * Copy rights, All rights reserved
 */

package appymood.lobe.soundsmartproto.musicBrowse.listComponents;

import appymood.lobe.R;

import android.content.Context;
import android.view.View;

/**
 * This class is extended by all portable list items
 */
public class ListGenericItem {

    protected Context context;

    // where can the item be
    public static enum ListItemTypeEnum{SECTION, SUB_SECTION, SINGLE_PLAYLIST_ITEM, SINGLE_lIB_ITEM};
    protected ListItemTypeEnum mlistItemType;

    protected int mPosition;

    // used for filtering in search mode
    protected boolean mIsTemporaryGone;
	
	public ListGenericItem(Context context, ListItemTypeEnum mlistItemType) {
		this.mlistItemType = mlistItemType;
		this.context = context;
	}
	
	public void adoptView(View v, int position)
	{
		mPosition = position;

        // show hide high level containers
		if (mIsTemporaryGone) {
            v.findViewById(R.id.sizePusher).setVisibility(View.GONE);
        } else {
            switch (mlistItemType) {
                case SECTION:
                case SUB_SECTION:
                case SINGLE_lIB_ITEM:
                    v.findViewById(R.id.sizePusher).setVisibility(View.GONE);
                    break;
                default:
                    // nothing to do
            }
        }
    }

    public void setmIsTemporaryGone(boolean isGone)
    {
        mIsTemporaryGone = isGone;
    }

    public boolean isFilterable(){
        return false;
    }

    public int getInflateViewID(){
        return R.layout.music_lib_row;
    }

    public String[] getStringFilter()
    {
        return null;
    }

	public boolean isSection() {
		return (mlistItemType == ListItemTypeEnum.SECTION || mlistItemType == ListItemTypeEnum.SUB_SECTION);
	}

}
